import boto3

groupName="GioE-187F94"
fIdre = r'(?<=functions\/)[\w+\-*]+(?=\/)'
fVersionre = r'(?<=versions\/)[\w+\-*]+'


def get_group_info(ggClient, groupName):
    res = ggClient.list_groups()
    groups = res['Groups']
    for group in groups:
        if group['Name'] == groupName:
            return group
    return None
    
def check_if_spooler_present(groupName):
    spooler_already_present = False
    client = boto3.client('greengrass')
    groupInfo = get_group_info(client, groupName)
    groupVersion = client.get_group_version(GroupId=groupInfo['Id'], GroupVersionId=groupInfo['LatestVersion'])
    groupVersion = groupVersion['Definition']
    funDefVersion = groupVersion['FunctionDefinitionVersionArn']
    print(funDefVersion)
    functionId = re.search(fIdre, funDefVersion)
    functionId = functionId.group(0)
    print(functionId)
    functionVersionId = re.search(fVersionre, funDefVersion)
    functionVersionId = functionVersionId.group(0)
    print(functionVersionId)
    version = client.get_function_definition_version(FunctionDefinitionId=functionId, FunctionDefinitionVersionId=functionVersionId)
    lambda_version = version['Definition']['Functions']
    for fun in lambda_version:
        if fun['FunctionArn'] == 'arn:aws:lambda:::function:GGCloudSpooler:1':
            spooler_already_present = True
            print("Funzione spooler già presente:\n"+str(fun)+"\n")
    return spooler_already_present
    
    
is_present = check_if_spooler_present(groupName)
if is_present:
    print("Spooler presente nella funzione del gruppo")
else:
    print("Spooler non presente nella funzione del gruppo") 
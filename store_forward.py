import boto3
import re

def get_group_info(ggClient, groupName):
    res = ggClient.list_groups()
    groups = res['Groups']
    for group in groups:
        if group['Name'] == groupName:
            return group
    return None

def create_and_deploy_group(ggClient, groupId, coreDefVersion="", functionDefVersion="", deviceDefVersion="", loggerDefVersion="", resDefVersion="", subscrDefVersion=""):
    newGroupVersion = ggClient.create_group_version(GroupId=groupId, CoreDefinitionVersionArn = coreDefVersion, FunctionDefinitionVersionArn=functionDefVersion, SubscriptionDefinitionVersionArn=subscrDefVersion, ResourceDefinitionVersionArn=resDefVersion)
    print("\n" + newGroupVersion['Version'] + "\n")
    newDeployment = ggClient.create_deployment(GroupId=groupId, GroupVersionId=newGroupVersion['Version'], DeploymentType='NewDeployment')
    return newDeployment

ggSpooler = {"FunctionArn": "arn:aws:lambda:::function:GGCloudSpooler:1","FunctionConfiguration": {"Environment": {"Variables":{"GG_CONFIG_MAX_SIZE_BYTES":"10485760","GG_CONFIG_STORAGE_TYPE":"FileSystem", "GG_CONFIG_SUBSCRIPTION_QUALITY":"AtLeastOncePersistent"}},"Executable": "spooler","MemorySize": 32768,"Pinned": True,"Timeout": 3},"Id": "spooler-function"}
fIdre = r'(?<=functions\/)[\w+\-*]+(?=\/)'
fVersionre = r'(?<=versions\/)[\w+\-*]+'
groupName = "GioE-187F94"    
ggClient = boto3.client('greengrass')
groupInfo = get_group_info(ggClient, groupName)
if groupInfo:
    print(groupInfo)
else:
    print("Greengrass group "+ groupName + " not found.")

groupVersion = ggClient.get_group_version(GroupId=groupInfo['Id'], GroupVersionId=groupInfo['LatestVersion'])
print(groupVersion['Definition'])
coreDefVersion = groupVersion['Definition']['CoreDefinitionVersionArn'] 
resDefVersion = groupVersion['Definition']['ResourceDefinitionVersionArn'] 
subDefVersion = groupVersion['Definition']['SubscriptionDefinitionVersionArn'] 
funDefVersion = groupVersion['Definition']['FunctionDefinitionVersionArn']
print(funDefVersion)
functionId = re.search(fIdre, funDefVersion)
functionId = functionId.group(0)
print(functionId)
functionVersionId = re.search(fVersionre, funDefVersion)
functionVersionId = functionVersionId.group(0)
print(functionVersionId)
version = ggClient.get_function_definition_version(FunctionDefinitionId=functionId, FunctionDefinitionVersionId=functionVersionId)
lambda_version = version['Definition']['Functions']
print(lambda_version)
spooler_already_present = False
for fun in lambda_version:
    if fun['FunctionArn'] == 'arn:aws:lambda:::function:GGCloudSpooler:1':
        spooler_already_present = True
        print("Funzione spooler già presente:\n"+str(fun)+"\n")
if not spooler_already_present:
    print("appendo spooler")
    lambda_version.append(ggSpooler)
print("\nNew Functions:")
print(lambda_version)
print("\n")
newLambdaVersion = ggClient.create_function_definition_version(FunctionDefinitionId=functionId, Functions=lambda_version)
print(newLambdaVersion['Arn'])
newGroupVersion = create_and_deploy_group(ggClient, groupInfo['Id'], coreDefVersion=coreDefVersion, functionDefVersion=newLambdaVersion['Arn'], subscrDefVersion=subDefVersion, resDefVersion=resDefVersion )
print("\nNuovo Deployment:\n" + str(newGroupVersion))

